﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using LS.Snet.Web.Spa;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Ajax.Utilities;
using System.Text.RegularExpressions;

namespace flexpack
{
    class Program
    {
        static void Main(string[] args)
        {
            var text = File.ReadAllText(args.First());
            print(text, args.Skip(1).ToArray());
        }
        async static Task print(string file, string[] args)
        {
            try
            {
                var code = "IEnumerable<PreBuilder> _(params string[] args) " +
                    $"{{{file}{Environment.NewLine}}} " +
                    $"return _({string.Join(",", args.Select(x => $"\"{x.Replace("\\", "\\\\").Replace("\"", "\\\"")}\""))});";
                var result = await CSharpScript.EvaluateAsync<IEnumerable<PreBuilder>>(code, ScriptOptions.Default
                    .AddImports("System.Collections.Generic")
                    .AddImports("System.Linq")
                    .AddImports("System")
                    .AddImports("LS.Snet.Web.Spa")
                    .AddImports("flexpack")
                    .AddImports("Microsoft.Ajax.Utilities")
                    .AddReferences(typeof(Builder).Assembly)
                    .AddReferences(typeof(PreBuilder).Assembly)
                    .AddReferences(typeof(Minifier).Assembly)
                    );
                foreach(var build in result)
                {
                    build.Invoke();
                }

            }
            catch(Exception ex)
            {
                Console.Write(ex);
            }
        }
    }
    public static class PreBuilderExt
    {
        public static PreBuilder Build<T, TT>(this Builder @this, Func<PreBuilder, TT> action) where T : PrebuildAction<TT>, new()
        {
            var self = (PreBuilder)@this;
            var t = new T();
            t.Action = action;
            self.Action(t);
            return self;
        }
    }
    public class PreBuilder : Builder
    {
        private readonly List<PrebuildAction> actions = new List<PrebuildAction>();

        public void Action(PrebuildAction action) => actions.Add(action);

        public void Invoke()
        {
            foreach (var act in actions)
                act.Invoke(this);
        }
    }
}
