﻿using System;
using System.IO;
using LS.Snet.Web.Spa;

namespace flexpack
{
    public abstract class PrebuildAction
    {
        public abstract void Invoke(PreBuilder builder);
    }
    public abstract class PrebuildAction<T> : PrebuildAction
    {
        public Func<PreBuilder, T> Action { get; set; }
    }
    public class WriteFile : PrebuildAction<string>
    {
        public override void Invoke(PreBuilder builder)
        {
            File.WriteAllText(Action(builder), builder.TextContent);
        }
    }
    public class MergeFiles : PrebuildAction<MergeFiles.Options>
    {
        public override void Invoke(PreBuilder builder)
        {
            var options = Action(builder);
            var root = new DirectoryInfo(options.folder);
            var files = root.GetFiles(options.pattern, options.all ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
            var path = new FileInfo(options.path);
            path.Directory.Create();
            path.Delete();
            foreach (var file in files)
            {
                builder.Name = file.FullName.Replace(root.FullName + "\\", string.Empty);
                File.AppendAllText(path.FullName, Environment.NewLine + builder.TextContent);
                builder.Bind("null", _ => null);
            }
        }
        public class Options
        {
            public string folder;
            public string pattern;
            public bool all;
            public string path;
        }
    }
    public class WriteFiles : PrebuildAction<WriteFiles.Options>
    {
        public override void Invoke(PreBuilder builder)
        {
            var options = Action(builder);
            var root = new DirectoryInfo(options.folder);
            var files = root.GetFiles(options.pattern, options.all ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
            foreach(var file in files)
            {
                builder.Name = file.FullName.Replace(root.FullName + "\\", string.Empty);
                var path = new FileInfo(options.template());
                path.Directory.Create();
                if (options.binary)
                    File.WriteAllBytes(path.FullName, builder.BinaryContent);
                else
                    File.WriteAllText(path.FullName, builder.TextContent);
                builder.Bind("null", _ => null);
            }
        }
        public class Options
        {
            public bool binary;
            public string folder;
            public string pattern;
            public bool all;
            public Func<string> template;
        }
    }
}
