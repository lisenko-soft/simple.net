﻿using LS.Snet.Web.Client;
using LS.Snet.Web.Social;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Snet.Tests
{
    [TestClass]
    public class SocialTest
    {
        const string token = "EAAEJ9qmQggMBAPWygiEPVW68FwWTTsbdtUDXMiV0N56Vh9rpHd7BZCQC190pPLC7ZADbr4jnbGZB7RswkFHQXUcPzEUtJtBSyME7GCg7OZAZCtbfiI1q5MijikioRCzRXO77SZCRJOZCvJ3p7F3j0wSIWlZAj6j6HkWZCXonGrFsVbcIXtTeFRZB7fKSfYZAVrmnEmCJwbicigFUgZDZD";
        [TestMethod]
        public void FacebookAuth()
        {
            var service = new AuthService(new SocialConfig[0]);
            var settings = new Http.Configurator
            {
                ParseT = (a,b)=> Newtonsoft.Json.JsonConvert.DeserializeObject(a,b)
            };
            var result = service.AuthFacebook(token, settings);
            Assert.AreEqual(true, result.Ok);
        }
    }
}
