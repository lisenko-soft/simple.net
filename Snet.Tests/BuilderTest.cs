﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LS.Snet.Web.Spa;
using System.IO;
using System.Diagnostics;

namespace Snet.Tests
{
    [TestClass]
    public class BuilderTest
    {
        [TestMethod]
        public void First()
        {
            var content = File.ReadAllText("test.html");
            var vars = new Dictionary<string, object>();
            vars["Style"] = Enumerable.Range(0, 10).Select(x=> "<link href=\"/app/resource/flags.css\" rel=\"stylesheet\" type=\"text/css\"/>").ToArray();
            vars["Template"] = Enumerable.Range(0, 20).Select(x => "<link href=\"/app/resource/flags.css\" rel=\"stylesheet\" type=\"text/css\"/>").ToArray();
            vars["Component"] = Enumerable.Range(0, 20).Select(x => "<link href=\"/app/resource/flags.css\" rel=\"stylesheet\" type=\"text/css\"/>").ToArray();
            vars["Theme"] = "fox";
            vars["Locale"] = null;
            Stopwatch first = new Stopwatch();
            first.Start();
            var old = content.FormatContent(vars);
            first.Stop();
            Stopwatch second = new Stopwatch();
            second.Start();
            var @new = content.FormatKiwami(x=> x.Vars = vars);
            second.Stop();
            Assert.AreEqual(old, @new);
            Assert.AreEqual(first.ElapsedMilliseconds < 10, second.ElapsedMilliseconds);
        }
    }
}
