﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LS.Snet.Web.Client;

namespace Snet.Tests
{
    [TestClass]
    public class HttpTest
    {
        [TestMethod]
        public void ArrayQuerryTest()
        {
            var a = new { method = "get" };
            const string result = "http://test.org/method/get?array=1&array=2&array=3&id=LS.test&count=228";
            var url = Http.Get("http://test.org/method/:method", new {a.method , array = new int[] { 1, 2, 3 }, id = "LS.test", count = 228 }).Url();
            Assert.AreEqual(result, url);
        }
        [TestMethod]
        public void LocalTest()
        {
            var resp = Http.Get("http://localhost:5000/lol").Run();
        }
    }
}
