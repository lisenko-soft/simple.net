﻿using System;
using System.Collections.Generic;
using System.Linq;
using LS.Snet.Web.Spa.Interfaces;

namespace LS.Snet.Web.Spa
{
    public class Builder : IApplication, IComponent, IResource, IBuilderSettings
    {
        private Func<Builder, Content> source;
        private readonly Dictionary<string, Func<Builder, object>> binders = new Dictionary<string, Func<Builder, object>>();
        private Func<string, string> map = a => a;
        public Builder Bind(Func<Builder, Content> source)
        {
            this.source = source ?? throw new ArgumentNullException(nameof(source));
            done = null;
            return this;
        }
        public Builder Bind(string name, Func<Builder, object> variable)
        {
            binders[name] = variable;
            done = null;
            return this;
        }
        public Builder Manual(Func<string, string> map)
        {
            this.map = map ?? throw new ArgumentNullException(nameof(map));
            done = null;
            return this;
        }
        public Builder Setup(Action<IBuilderSettings> action)
        {
            action(this);
            return this;
        }
        public string Name { get; set; }
        public string Selector { get; set; } = "${0}";
        public string Charset { get; set; }
        public void Set(string name, object val) => Bind(name, _ => val);
        public T Get<T>(string name)
        {
            try
            {
                return (T)binders[name](this);
            }
            catch
            {
                return default(T);
            }
        }
        private Content done = null;
        public string ContentType => Content.ContentType;
        public string TextContent => Content.GetText();
        public byte[] BinaryContent => Content.GetBytes();
        public bool NoContent => Content.NoContent;
        public Content Content
        {
            get
            {
                if (done != null)
                    return done;
                var content = source(this);
                var retval = content.Copy();
                if (!string.IsNullOrEmpty(Charset))
                    retval.ContentType += $"; charset={Charset}";
                if (content.NoContent || content.GetText == null)
                    return retval;
                retval.GetText = () => map(content.GetText().FormatKiwami(format => format.Vars = binders.ToDictionary(x => x.Key, x => x.Value(this))));
                retval.GetBytes = content.GetBytes;
                done = retval;
                return retval;
            }
        }
    }
}

