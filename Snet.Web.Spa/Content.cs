﻿using System;
using System.IO;

namespace LS.Snet.Web.Spa
{
    public sealed class Content : ICloneable
    {
        public Func<string> GetText { get; set; }
        public Func<byte[]> GetBytes { get; set; }
        public string ContentType { get; set; }
        public bool NoContent { get; set; }
        public static Content FromFileInfo(FileInfo file) => new Content
        {
            ContentType = file.Extension.GetMimeType(),
            NoContent = !file.Exists,
            GetBytes = () => File.ReadAllBytes(file.FullName),
            GetText = () => File.ReadAllText(file.FullName)
        };
        public static Content FromFile(string fileName) => FromFileInfo(new FileInfo(fileName));
        public Content Copy() => new Content
        {
            NoContent = NoContent,
            ContentType = ContentType
        };

        public object Clone()
        {
            return Copy();
        }
    }

}

