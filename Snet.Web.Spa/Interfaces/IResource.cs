﻿namespace LS.Snet.Web.Spa.Interfaces
{
    public interface IResource
    {
        string Name { get; set; }
        void Set(string name, object val);
        byte[] BinaryContent { get; }
        string ContentType { get; }
        bool NoContent { get; }
    }
}
