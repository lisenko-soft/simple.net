﻿namespace LS.Snet.Web.Spa.Interfaces
{
    public interface IComponent
    {
        string Name { get; set; }
        void Set(string name, object val);
        string TextContent { get; }
        string ContentType { get; }
        bool NoContent { get; }
    }
}
