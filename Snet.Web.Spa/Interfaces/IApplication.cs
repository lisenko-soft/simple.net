﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LS.Snet.Web.Spa.Interfaces
{
    public interface IApplication
    {
        void Set(string name, object val);
        string TextContent { get; }
        string ContentType { get; }
    }
}
