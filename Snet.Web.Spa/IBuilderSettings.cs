﻿namespace LS.Snet.Web.Spa
{
    public interface IBuilderSettings
    {
        string Selector { get; set; }
        string Charset { get; set; }
    }
}