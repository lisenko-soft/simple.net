﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace LS.Snet.Web.Spa
{
    public static class Binder
    {
        public static Builder UseFiles(this Builder builder, string template) => builder.Bind(x => Content.FromFile(string.Format(template, x.Name)));
        public static Builder UseFile(this Builder builder, string filename) => builder.Bind(x => Content.FromFile(filename));
        public static Builder UseInjector(this Builder builder, string keyword, string folder, string pattern, bool all, string template, Func<string, string> name = null)
        {
            Func<string, string> def = a => a;
            builder.Bind(keyword, _ =>
            {
                var root = new DirectoryInfo(folder);
                var files = root.GetFiles(pattern, all ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
                if (template.Contains("{1}"))
                    return files.Select(x => string.Format(template, (name ?? def).Invoke(x.FullName.Replace(root.FullName + "\\", string.Empty)), File.ReadAllText(x.FullName))).ToArray();
                else
                    return files.Select(x => string.Format(template, (name ?? def).Invoke(x.FullName.Replace(root.FullName + "\\", string.Empty)))).ToArray();
            });
            return builder;
        }
    }

}

