﻿namespace LS.Snet.MDocer
{
    public struct DocerTemplateCollection
    {
        public string Page { get; set; }
        public string Param { get; set; }
        public string Response { get; set; }
        public string Home { get; set; }
    }
}
