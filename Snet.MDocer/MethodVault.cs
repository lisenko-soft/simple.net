﻿using System.Reflection;

namespace LS.Snet.MDocer
{
    internal struct MethodVault
    {
        public DocerAttribute DocerAttribute { get; set; }
        public MethodInfo MethodInfo { get; set; }
        public DocMethod DocMethod { get; set; }
    }
}
