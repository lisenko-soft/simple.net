﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LS.Snet.MDocer
{
    public class DocGroup
    {
        public string Name { get; set; }
        public DocMethod[] Method { get; set; }

    }
    public class DocMethod
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DocParam[] Param { get; set; }
        public object GenerateBody(Type type) => create(type);
        public DocResponse Returns { get; set; } = new DocResponse
        {
            Status = 200,
            Description = "OK"
        };
        public DocResponse[] Response { get; set; } = new DocResponse[0];
        public IEnumerable<DocResponse> GenerateResponse(Type type)
        {
            var resp = Response.AsEnumerable();
            if ((type ?? typeof(void)) != typeof(void))
            {
                Returns.Body = Returns.Body ?? create(type);
                resp = resp.Concat(new DocResponse[] { Returns });
            }
            return resp.OrderBy(x=>x.Status);
        }
        private readonly List<Type> safe = new List<Type>();
        private object create(Type type)
        {
            if (type.IsEnum)
                return Enum.GetNames(type);
            var real = Utils.GetRealType(type);
            if(real.FullName.StartsWith("System") || safe.Contains(real))
                return Utils.GetRealTypeName(type);
            safe.Add(real);
            var properties = real.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => x.CanRead && x.GetCustomAttribute<DocerIgnoreAttribute>() == null && !x.GetCustomAttributes().Any(xx=> xx.GetType().Name == "JsonIgnoreAttribute"));
            var dic = properties.ToDictionary(prop => prop.Name, prop => create(prop.PropertyType));
            if (real != type)
                return new object[] { dic };
            else
                return dic;
        }
    }

    public class DocParam
    {
        public bool Required { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class DocResponse
    {
        public int Status { get; set; }
        public string Description { get; set; }
        public object Body { get; set; }
    }
}
