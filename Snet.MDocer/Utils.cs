﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LS.Snet.MDocer
{
    internal static class Utils
    {
        internal class FormatOptions
        {
            public Dictionary<string, object> Vars { get; set; }
            public object Object
            {
                set
                {
                    Vars = value?.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public).ToDictionary(x => x.Name, x => x.GetValue(value)) 
                        ?? new Dictionary<string, object>();
                }
            }
            public string Selector { get; set; } = "${0}";
            public string Join { get; set; } = Environment.NewLine;
            public string Null { get; set; } = null;
            public FormatOptions(Action<FormatOptions> action)
            {
                action(this);
            }
        }

        internal static string FormatKiwami(this string content, Action<FormatOptions> options)
        {
            if (string.IsNullOrEmpty(content))
                return content;
            var format = new FormatOptions(options);
            if (!format.Vars.Any())
                return content;
            var mapper = format.Vars.ToDictionary(x => string.Format(format.Selector, x.Key), x => x.Value);
            return string.Join(string.Empty, content.SplitAndKeep(mapper.Keys.ToArray()).Select(x =>
            {
                if (!mapper.ContainsKey(x))
                    return x;
                var val = mapper[x];
                if (val is System.Collections.IList)
                    return string.Join(format.Join, (val as System.Collections.IList).Cast<object>().Select(xx => xx?.ToString() ?? format.Null));
                return mapper[x]?.ToString() ?? format.Null;
            }));
        }
        public static IEnumerable<string> SplitAndKeep(this string s, params string[] delims)
        {
            var rows = new List<string>() { s };
            foreach (string delim in delims)//delimiter counter
            {
                for (int i = 0; i < rows.Count; i++)//row counter
                {
                    int index = rows[i].IndexOf(delim);
                    if (index > -1
                        && rows[i].Length > index + 1)
                    {
                        string leftPart = rows[i].Substring(0, index);
                        string rightPart = rows[i].Substring(index + delim.Length);
                        rows[i] = leftPart;
                        rows.Insert(i + 1, delim);
                        rows.Insert(i + 2, rightPart);
                        i++;
                    }
                }
            }
            return rows;
        }

        internal static string GetRoute(this MethodInfo method)
        {
            var mrouteattr = method.GetCustomAttributes().FirstOrDefault(x => x.GetType().Name == "RouteAttribute");
            var crouteattr = method.DeclaringType.GetCustomAttributes().FirstOrDefault(x => x.GetType().Name == "RouteAttribute");
            var route = "/";

            if (crouteattr != null)
                route += (string)crouteattr.GetType().GetProperty("Template").GetValue(crouteattr);
            if (mrouteattr != null)
            {
                var mroute = (string)mrouteattr.GetType().GetProperty("Template").GetValue(mrouteattr);
                if (route.First() == '~')
                    route = mroute.Substring(1);
                else
                    route += '/' + mroute;
            }
            return route;
        }
        internal static Type GetRealType(this Type type)
        {
            if (type.IsArray)
                return type.GetElementType();
            if (type.GetInterface("IEnumerable") != null && type != typeof(string))
                return type.GetGenericArguments()[0];
            return type;
        }
        internal static string GetRealTypeName(this Type type)
        {
            if (type.IsArray)
                return GetRealTypeName(type.GetElementType()) + "[]";
            if (type.GetInterface("IEnumerable") != null && type != typeof(string))
            {
                var genargs = type.GetGenericArguments();
                if (genargs.Length == 2)
                    return GetRealTypeName(genargs[1]) + $"[{GetRealTypeName(genargs[0])}]";
                return genargs[0].Name + "[]";
            }
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(int?).GetGenericTypeDefinition())
                return GetRealTypeName(type.GetGenericArguments()[0]) + "?";
            if (type.IsGenericType)
                return type.Name.Split('`').First() + "<" + string.Join(",", type.GetGenericArguments().Select(x=> GetRealTypeName(x))) + ">";
            return type.Name;
        }
    }
}
