﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LS.Snet.MDocer
{
    public class Docer
    {
        private readonly DocerTemplateCollection template;
        private readonly DocGroup[] groups;
        private readonly Type[] controllers;
        private readonly string endpoint;
        private readonly Func<object, string> serialize;
        private readonly MethodVault[] methods;

        public Docer(DocerTemplateCollection template, DocGroup[] groups, Type[] controllers, string endpoint, Func<object, string> serialize = null)
        {
            this.template = template;
            this.groups = groups ?? throw new ArgumentNullException(nameof(groups));
            this.controllers = controllers;
            this.endpoint = endpoint ?? throw new ArgumentNullException(nameof(endpoint));
            this.serialize = serialize;
            methods = controllers.SelectMany(x => x.GetMethods().Where(xx => xx.GetCustomAttribute<DocerAttribute>() != null))
                .Select(x =>
                {
                    var attr = x.GetCustomAttribute<DocerAttribute>();
                    var doc = groups.Single(xx => xx.Name == attr.Group).Method.Single(xx => xx.Name == attr.Method);
                    return new MethodVault
                    {
                        DocerAttribute = attr,
                        DocMethod = doc,
                        MethodInfo = x
                    };
                }).ToArray();
        }

        public override string ToString()
        {
            throw new NotImplementedException();
        }
        public string ToString(string group, string method)
        {
            var vault = methods.SingleOrDefault(x => x.DocerAttribute.Method == method && x.DocerAttribute.Group == group);
            if (vault.MethodInfo == null)
                return null;
            var vars = new Dictionary<string, object>();
            vars["method_group"] = vault.DocerAttribute.Group;
            vars["method_name"] = vault.DocerAttribute.Method;
            vars["method_route"] = vault.MethodInfo.GetRoute();
            vars["method_description"] = vault.DocMethod.Description;
            vars["method_type"] = string.Join(", ", vault.MethodInfo.GetCustomAttributes().Where(x => x.GetType().BaseType.Name == "HttpMethodAttribute").Select(x => x.GetType().Name.Replace("Http", string.Empty).Replace("Attribute", string.Empty).ToUpper()));
            vars["method_url"] = endpoint + vars["method_route"];
            var body = vault.MethodInfo.GetParameters().SingleOrDefault(x => x.GetCustomAttributes().Any(xx => xx.GetType().Name == "FromBodyAttribute"));
            vars["method_body"] = body == null ? null : (serialize?.Invoke(vault.DocMethod.GenerateBody(body.ParameterType)) ?? body.ParameterType.ToString());
            var response = vault.DocMethod.GenerateResponse(vault.DocerAttribute.Returns);
            vars["method_response"] = string.Join(Environment.NewLine, response.Select(x => ResponseToString(x)));
            vars["method_param"] = string.Join(Environment.NewLine, vault.MethodInfo.GetParameters()
                .Where(x => !x.GetCustomAttributes().Any(xx => xx.GetType().Name == "FromBodyAttribute" || xx.GetType().Name == "FromHeaderAttribute"))
                .SelectMany(param =>
                {
                    if (param.ParameterType.GetRealType().FullName.StartsWith("System"))
                        return new string[] { ParamToString(vault, param) };
                    else
                        return param.ParameterType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                        .Where(x => x.CanRead && x.CanWrite && !x.GetCustomAttributes().Any(xx => xx.GetType().Name == "JsonIgnoreAttribute"))
                        .Select(x => ParamToString(vault, param, x));
                }));
            return template.Page.FormatKiwami(x=> { x.Vars = vars; x.Selector = "%{0}%"; });
        }
        private string ParamToString(MethodVault vault, ParameterInfo parameter, PropertyInfo property = null)
        {
            var param = vault.DocMethod.Param.SingleOrDefault(x => x.Name == (property?.Name ?? parameter.Name));
            var vars = new Dictionary<string, object>();
            vars["param_value"] = param?.Required == true ? "required" : "optional";
            vars["param_type"] = Utils.GetRealTypeName(property?.PropertyType ?? parameter.ParameterType);
            vars["param_origin"] = parameter.GetCustomAttributes().SingleOrDefault(x => x.GetType().GetInterface("IBindingSourceMetadata") != null)?.GetType().Name.Replace("From", string.Empty).Replace("Attribute", string.Empty).ToLower();
            vars["param_name"] = property?.Name ?? parameter.Name;
            vars["param_description"] = param?.Description;
            return template.Param.FormatKiwami(x => { x.Vars = vars; x.Selector = "%{0}%"; });
        }
        private string ResponseToString(DocResponse response)
        {
            var vars = new Dictionary<string, object>();
            vars["response_status"] = response.Status;
            vars["response_description"] = response.Description;
            vars["response_body"] = serialize?.Invoke(response.Body) ?? response.Body?.ToString();
            return template.Response.FormatKiwami(x => { x.Vars = vars; x.Selector = "%{0}%"; });
        }
        public KeyValuePair<string, KeyValuePair<string, string>[]>[] Tree =>
            groups.Select(x => new KeyValuePair<string, KeyValuePair<string, string>[]>(x.Name,
                x.Method.Select(xx => new KeyValuePair<string, string>(xx.Name, xx.Description)).ToArray())).ToArray();
    }
}
