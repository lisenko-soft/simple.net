﻿using System;

namespace LS.Snet.MDocer
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class DocerAttribute : Attribute
    {
        public DocerAttribute( string group, string method)
        {
            Group = group ?? throw new ArgumentNullException(nameof(group));
            Method = method ?? throw new ArgumentNullException(nameof(method));
        }
        public Type Returns { get; set; }

        public string Method { get; }
        public string Group { get; }
    }
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class DocerIgnoreAttribute : Attribute { }
}
