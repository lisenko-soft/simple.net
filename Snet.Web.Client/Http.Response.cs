﻿using System;
using System.Net;

/// <summary>
/// LISENKO SOFT Simple Net
/// </summary>
namespace LS.Snet.Web.Client
{
    public partial class Http
    {
        /// <summary>
        /// Http Response
        /// </summary>
        /// <typeparam name="T">Body type</typeparam>
        public sealed class Response<T>
        {
            /// <summary>
            /// Request url
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// Response status
            /// </summary>
            public bool Ok { get; set; }
            /// <summary>
            /// Response HttpStatusCode
            /// </summary>
            public HttpStatusCode Status { get; set; }
            /// <summary>
            /// Response Status text
            /// </summary>
            public string StatusText { get; set; }
            /// <summary>
            /// Response Http Headers
            /// </summary>
            public WebHeaderCollection Headers { get; set; }
            /// <summary>
            /// Response body as text
            /// </summary>
            public string BodyText { get; set; }
            /// <summary>
            /// Response body as T
            /// </summary>
            public T Body { get; set; }
        }
    }
}