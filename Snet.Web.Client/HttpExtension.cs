﻿using System;
using System.Net;
using System.Text;
/// <summary>
/// LISENKO SOFT Simple Net
/// </summary>
namespace LS.Snet.Web.Client
{
    public static class HttpExtension
    {
        public static Http Configure<TConfigurator>(this Http self) where TConfigurator : Http.Configurator, new() => self.Configure(new TConfigurator());
        public static Http ConfigureHeaders(this Http self, Action<WebHeaderCollection> action)
            => self.Configure(new Http.Configurator { Headers = action });
        public static Http ConfigureContentType(this Http self, string contentType)
            => self.Configure(new Http.Configurator { ContentType = contentType });
        public static Http ConfigureEncoding(this Http self, Encoding request = null, Encoding response = null)
            => self.Configure(new Http.Configurator { RequestEncoding = request, ResponseEncoding = response });
        public static Http ConfigureParsing(this Http self, Func<object, string> request = null, Func<string, Type, object> response = null)
            => self.Configure(new Http.Configurator { Parse = request, ParseT = response });
        public static Http ConfigureDynamicParsing(this Http self, Func<object, string> request = null, Func<string, object> response = null)
            => self.Configure(new Http.Configurator { Parse = request, ParseD = response });
        public static Http ConfigureLogging(this Http self, Action<string> debug = null, Func<Exception, bool> error = null)
            => self.Configure(new Http.Configurator { LogD = debug, LogE = error });
    }
}