﻿using System;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

/// <summary>
/// LISENKO SOFT Simple Net
/// </summary>
namespace LS.Snet.Web.Client
{
    public partial class Http
    {
        /// <summary>
        /// Params to configure
        /// </summary>
        public class Configurator
        {
            public static Configurator Global { get; } = new Configurator
            {
                RequestEncoding = Encoding.Default,
                ResponseEncoding = Encoding.UTF8,
                Parse = a => null,
                ParseD = a => null,
                ParseT = (a, b) => null
            };

            /// <summary>
            /// Endpoint
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// Http Method
            /// </summary>
            public string Method { get; set; }
            /// <summary>
            /// Default Request Body Encoding
            /// </summary>
            public Encoding RequestEncoding { get; set; }
            /// <summary>
            /// Default Response Body Encoding
            /// </summary>
            public Encoding ResponseEncoding { get; set; }
            /// <summary>
            /// Query (:param) and GET params
            /// </summary>
            public object Params { get; set; }
            /// <summary>
            /// Body object
            /// </summary>
            public object Body { get; set; }
            /// <summary>
            /// Http content type
            /// </summary>
            public string ContentType { get; set; }
            /// <summary>
            /// Configure Http headers
            /// </summary>
            public Action<WebHeaderCollection> Headers { get; set; }
            /// <summary>
            /// Deserialize body to dynamic object
            /// </summary>
            public Func<string, object> ParseD { get; set; }
            /// <summary>
            /// Serialize object to text body
            /// </summary>
            public Func<object, string> Parse { get; set; }
            /// <summary>
            /// Deserialize body to typed object
            /// </summary>
            public Func<string, Type, object> ParseT { get; set; }
            /// <summary>
            /// Logging shit Debug
            /// </summary>
            public Action<string> LogD { get; set; }
            /// <summary>
            /// Logging shit Error //returns ignore exception
            /// </summary>
            public Func<Exception, bool> LogE { get; set; }
            /// <summary>
            /// Get request Url
            /// </summary>
            /// <returns>url</returns>
            public string GetUrl()
            {
                var p = Params;
                var url = Url.Replace('\\','/');
                var dic = p?.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Select(x => new KeyValuePair<string, KeyValuePair<bool,object>>(x.Name, 
                    new KeyValuePair<bool, object>(url.Contains("/:" + x.Name), x.GetValue(p)))) 
                    ?? Enumerable.Empty<KeyValuePair<string, KeyValuePair<bool, object>>>();
                var query = string.Join("&", dic.Where(x => !x.Value.Key)
                    .SelectMany(x => (x.Value.Value as System.Collections.IList)?.Cast<object>()
                    .Select(y => $"{x.Key}={y}") ?? new string[] { $"{x.Key}={x.Value.Value}" }));
                if (!string.IsNullOrEmpty(query))
                    query = '?' + query;
                foreach (var lol in dic.Where(x => x.Value.Key))
                    url = url.Replace(":" + lol.Key, "" + lol.Value.Value);
                return url + query;
            }
        }
    }
}