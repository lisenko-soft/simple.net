﻿/// <summary>
/// LISENKO SOFT Simple Net
/// </summary>
namespace LS.Snet.Web.Client
{
    public partial class Http
    {
        /// <summary>
        /// Binary file representation
        /// </summary>
        public class File
        {
            /// <summary>
            /// Binary data
            /// </summary>
            public byte[] Binary { get; set; }
        }
    }
}