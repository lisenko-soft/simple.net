﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

/// <summary>
/// LISENKO SOFT Simple Net
/// </summary>
namespace LS.Snet.Web.Client
{
    public partial class Http
    {
        private readonly Configurator configurator;
        private readonly WebHeaderCollection headers;

        private Http(string url, string method)
        {
            headers = new WebHeaderCollection();
            configurator = new Configurator { Url = url, Method = method };
            Configure(Configurator.Global);
        }

        /// <summary>
        /// Get request
        /// </summary>
        /// <param name="url">Endpoint</param>
        public static Http Get(string url) => new Http(url, "GET");

        /// <summary>
        /// Get request
        /// </summary>
        /// <param name="url">Endpoint</param>
        /// <param name="param">Get params</param>
        public static Http Get(string url, object param) => Get(url).Configure(new Configurator { Params = param });

        /// <summary>
        /// Post request
        /// </summary>
        /// <param name="url">Endpoint</param>
        public static Http Post(string url) => new Http(url, "POST");

        /// <summary>
        /// Post request
        /// </summary>
        /// <param name="url">Endpoint</param>
        /// <param name="body">Post body</param>
        public static Http Post(string url, object body) => Post(url).Configure(new Configurator { Body = body });

        /// <summary>
        /// Post request
        /// </summary>
        /// <param name="url">Endpoint</param>
        /// <param name="body">Post body</param>
        /// <param name="param">Get params</param>
        /// <returns></returns>
        public static Http Post(string url, object body, object param) => Post(url, body).Configure(new Configurator { Params = param });

        /// <summary>
        /// Configure request
        /// </summary>
        public Http Configure(Configurator configurator)
        {
            configurator.Headers?.Invoke(headers);

            if (configurator.Params != null)
                this.configurator.Params = configurator.Params;

            if (configurator.Body != null)
                this.configurator.Body = configurator.Body;

            if (configurator.ContentType != null)
                this.configurator.ContentType = configurator.ContentType;

            if (configurator.Method != null)
                this.configurator.Method = configurator.Method;

            if (configurator.RequestEncoding != null)
                this.configurator.RequestEncoding = configurator.RequestEncoding;

            if (configurator.ResponseEncoding != null)
                this.configurator.ResponseEncoding = configurator.ResponseEncoding;

            if (configurator.Parse != null)
                this.configurator.Parse = configurator.Parse;

            if (configurator.ParseD != null)
                this.configurator.ParseD = configurator.ParseD;

            if (configurator.ParseT != null)
                this.configurator.ParseT = configurator.ParseT;

            if (configurator.LogD != null)
                this.configurator.LogD = configurator.LogD;

            if (configurator.LogE != null)
                this.configurator.LogE = configurator.LogE;

            return this;
        }
        /// <summary>
        /// Finalize request and run asynchronously
        /// </summary>
        public Task<Response<dynamic>> RunAsync() => Task.Run(() => run<dynamic>(true));

        /// <summary>
        /// Finalize request and run
        /// </summary>
        public Response<dynamic> Run() => run<dynamic>(true);

        /// <summary>
        /// Finalize request and run asynchronously
        /// </summary>
        public Task<Response<T>> RunAsync<T>() => Task.Run(() => run<T>(false));

        /// <summary>
        /// Finalize request and run
        /// </summary>
        public Response<T> Run<T>() => run<T>(false);

        /// <summary>
        /// Finalize but get url only
        /// </summary>
        public string Url() => configurator.GetUrl();

        private Response<T> run<T>(bool dynamic)
        {
            object body;
            HttpStatusCode statusCode;
            string statusDescription;
            string url = configurator.GetUrl();
            bool ok;
            WebHeaderCollection headers;
            try
            {
                configurator.LogD?.Invoke($"[Snet.Web.Client] Processing {configurator.Method} {url}");
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
                myRequest.Headers = this.headers;
                myRequest.Method = configurator.Method;
                if (configurator.ContentType != null)
                    myRequest.ContentType = configurator.ContentType;
                if (configurator.Body != null)
                    using (var streamWriter = new StreamWriter(myRequest.GetRequestStream(), configurator.RequestEncoding))
                    {
                        streamWriter.Write(configurator.Parse(configurator.Body));
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                using (HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse())
                {
                    var stream = myResponse.GetResponseStream();
                    if (typeof(T) == typeof(File))
                        using (MemoryStream ms = new MemoryStream())
                        {
                            stream.CopyTo(ms);
                            body = ms.ToArray();
                        }
                    else
                        using (StreamReader reader = new StreamReader(stream, configurator.ResponseEncoding))
                            body = reader.ReadToEnd();
                    statusCode = myResponse.StatusCode;
                    statusDescription = myResponse.StatusDescription;
                    headers = myResponse.Headers;
                }
                ok = true;
            }
            catch (WebException ex)
            {
                if (ex.Response == null)
                {
                    if (configurator.LogE?.Invoke(ex) == true)
                        return new Response<T>();
                    throw ex;
                }
                var myResponse = (HttpWebResponse)ex.Response;
                using (var stream = myResponse.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    body = reader.ReadToEnd();
                    statusCode = myResponse.StatusCode;
                    statusDescription = myResponse.StatusDescription;
                    headers = myResponse.Headers;
                }
                ok = false;
            }
            catch (Exception ex)
            {
                if (configurator.LogE?.Invoke(ex) == true)
                    return new Response<T>();
                throw ex;
            }
            object b;
            try
            {
                if (body is byte[])
                    b = new File
                    {
                        Binary = body as byte[]
                    };
                else
                    b = dynamic ? configurator.ParseD(body as string) : configurator.ParseT(body as string, typeof(T));
                configurator.LogD?.Invoke($"[Snet.Web.Client] Result for {configurator.Method} {url} is {statusCode} {statusDescription}{Environment.NewLine}{body as string}");
            }
            catch (Exception ex)
            {
                if (configurator.LogE?.Invoke(ex) == true)
                    return new Response<T>();
                throw ex;
            }
            return new Response<T>
            {
                Headers = headers,
                BodyText = body as string,
                Ok = ok,
                Status = statusCode,
                StatusText = statusDescription,
                Url = url,
                Body = (T)b
            };
        }
    }
}