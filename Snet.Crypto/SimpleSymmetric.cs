﻿using System.Text;
using System.Security.Cryptography;

/// <summary>
/// LISENKO SOFT Simple Cryptography
/// </summary>
namespace LS.Snet.Crypto
{
    public class SimpleSymmetric<T> where T : SymmetricAlgorithm
    {
        /// <summary>
        /// Encode text
        /// </summary>
        /// <param name="text">Text to hash</param>
        /// <param name="encoding">Text encoding</param>
        /// <returns>Hash</returns>
        public static SimpleSymmetric<T> Encrypt(string text, Encoding encoding = null)
        {
            return new SimpleSymmetric<T>((encoding ?? Encoding.UTF8)?.GetBytes(text ?? ""), true);
        }
        /// <summary>
        /// Encode text
        /// </summary>
        /// <param name="text">Text to hash</param>
        /// <param name="encoding">Text encoding</param>
        /// <returns>Hash</returns>
        public static SimpleSymmetric<T> Decrypt(string text, Encoding encoding = null)
        {
            return new SimpleSymmetric<T>((encoding ?? Encoding.UTF8)?.GetBytes(text ?? ""), false);
        }
        private SimpleSymmetric(object o, bool encrypt)
        {
            SymmetricAlgorithm alg = SymmetricAlgorithm.Create(typeof(T).FullName);
            if (encrypt)
            {
            }
            else
            {

            }
        }

    }
}