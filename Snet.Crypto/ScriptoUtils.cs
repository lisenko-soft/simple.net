﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;

/// <summary>
/// LISENKO SOFT Simple Cryptography
/// </summary>
namespace LS.Snet.Crypto
{
    public static class ScriptoUtils
    {
        /// <summary>
        /// Public random generator
        /// </summary>        
        public static Random Random { get; } = new Random();
        private static Dictionary<string, List<string>> _ids = new Dictionary<string, List<string>>();
        private static List<string> ids(string set)
        {
            if (!_ids.ContainsKey(set))
                _ids.Add(set, new List<string>());
            return _ids[set];
        }
        public static bool SafeIds { get; set; } = true;
        public static void ClearId(string set, string id = null)
        {
            if (id == null)
                _ids.Remove(set);
            else
                ids(set).Remove(id);
        }
        public static string GenerateId(int id = 5, string set = null)
        {
            string nid = SimpleHash<SHA1>.Use(Random.Next() + "", Encoding.UTF8).Base64.Substring(0, id);
            if (!SafeIds)
                return nid;
            if (nid.Contains("+") || nid.Contains("/"))
                return GenerateId(id, set);
            if (set == null)
                return nid;
            if (ids(set).Contains(nid))
                return GenerateId(id, set);
            ids(set).Add(nid);
            return nid;
        }
    }
}