﻿namespace LS.Snet.Web.Social.Facebook
{
    public class Data
    {
        public int height { get; set; }
        public bool is_silhouette { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }

}
