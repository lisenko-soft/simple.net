﻿namespace LS.Snet.Web.Social.Facebook
{
    public class FacebookUser : IAuthUser
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public Picture picture { get; set; }
        public string id { get; set; }

        public string UserIcon => picture.data.url;

        public string UserId => id;

        public string UserName => first_name + " " + last_name;
    }

}
