﻿namespace LS.Snet.Web.Social
{
    public interface IAuthUser
    {
        string UserIcon { get; }
        string UserId { get; }
        string UserName { get; }
    }
}
