﻿namespace LS.Snet.Web.Social
{
    public class SocialNet
    {
        public string Icon { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
    }
}
