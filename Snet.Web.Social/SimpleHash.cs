﻿using System;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace LS.Snet.Web.Social
{
    internal class SimpleHash<T> where T : HashAlgorithm
    {
        /// <summary>
        /// Get Hash of Text
        /// </summary>
        /// <param name="text">Text to hash</param>
        /// <param name="encoding">Text encoding</param>
        /// <returns>Hash</returns>
        public static SimpleHash<T> Use(string text, Encoding encoding = null)
        {
            return new SimpleHash<T>((encoding ?? Encoding.UTF8)?.GetBytes(text ?? ""));
        }
        /// <summary>
        /// Get Hash of Data
        /// </summary>
        /// <param name="text">Text to hash</param>
        /// <param name="encoding">Text encoding</param>
        /// <returns>Hash</returns>
        public static SimpleHash<T> Use(byte[] data)
        {
            return new SimpleHash<T>(data);
        }
        public byte[] Hash { get; private set; }
        private SimpleHash(byte[] bytes)
        {
            HashAlgorithm alg = typeof(T).GetMethods(BindingFlags.Static | BindingFlags.Public)
                .FirstOrDefault(x => !x.GetParameters().Any())?.Invoke(null, new object[0]) as HashAlgorithm
                ?? HashAlgorithm.Create(typeof(T).FullName);
            Hash = alg.ComputeHash(bytes);
        }
        /// <summary>
        /// Get Hash in Hex format
        /// </summary>
        public string Hex => string.Join(string.Empty, Hash.Select(x => x.ToString("x2")));
        /// <summary>
        /// Get Hash in Base64 format
        /// </summary>
        public string Base64 => Convert.ToBase64String(Hash);
    }
}
