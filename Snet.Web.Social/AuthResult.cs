﻿using System;

namespace LS.Snet.Web.Social
{
    public struct AuthResult
    {
        public SocialConfig Social { get; set; }
        public IAuthUser User { get; set; }
        public Exception Error { get; set; }
        public bool Ok => User != null && Error == null;
        public string UserId => $"{Social.Id}:{User.UserId}";
    }
}
