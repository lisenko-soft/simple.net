﻿namespace LS.Snet.Web.Social.Vk
{
    internal class VkResult<TResponse>
    {
        public VkError error { get; set; }
        public TResponse response { get; set; }
    }
}
