﻿namespace LS.Snet.Web.Social.Vk
{
    internal class VkError
    {
        public int error_code { get; set; }
        public string error_msg { get; set; }
    }
}
