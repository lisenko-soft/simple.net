﻿namespace LS.Snet.Web.Social.Vk
{
    public class VkUser : IAuthUser
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string photo_100 { get; set; }

        public string UserId => id + "";

        public string UserName => first_name + " " + last_name;

        public string UserIcon => photo_100;
    }
}
