﻿namespace LS.Snet.Web.Social.Vk
{
    internal class VkArray<TElement>
    {
        public int count { get; set; }
        public TElement[] items { get; set; }
    }
}
