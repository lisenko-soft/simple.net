﻿namespace LS.Snet.Web.Social.Google
{
    public class GoogleUser : IAuthUser
    {
        public string sub { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string link { get; set; }
        public string picture { get; set; }
        public string gender { get; set; }
        public string locale { get; set; }

        public string UserId => sub;

        public string UserName => name;

        public string UserIcon => picture;
    }
}
