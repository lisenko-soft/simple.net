﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LS.Snet.Web.Social.Google
{
    internal class GoogleResult
    {
        public string access_token { get; set; }
        public string error { get; set; }
        public string error_description { get; set; }
    }
}
