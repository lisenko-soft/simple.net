﻿namespace LS.Snet.Web.Social.Ok
{
    public class OkUser : IAuthUser
    {
        public string uid { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string pic_3 { get; set; }

        public string UserId => uid;

        public string UserName => first_name + " " + last_name;

        public string UserIcon => pic_3;
    }

}
