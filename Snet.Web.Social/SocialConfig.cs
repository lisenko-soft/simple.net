﻿using System.Collections.Generic;

namespace LS.Snet.Web.Social
{
    public class SocialConfig
    {
        public string Id { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
        public string Api { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Token { get; set; } = new Dictionary<string, string>();
        public string Version { get; set; }
        public SocialNet ViewModel => new SocialNet
        {
            Icon = Icon,
            Url = Url,
            Name = Name
        };
        public static SocialConfig DefaultVk => new SocialConfig { Id = "vk", Api = "https://api.vk.com/method", Version = "5.52" };
        public static SocialConfig DefaultFacebook => new SocialConfig { Id = "facebook", Api = "https://graph.facebook.com" };
        public static SocialConfig DefaultGoogle => new SocialConfig { Id = "google", Api = "https://www.googleapis.com/oauth2/:Version", Version = "v3" };
        public static SocialConfig DefaultOk => new SocialConfig { Id = "ok", Api = "https://api.ok.ru" };
    }
}
