﻿using LS.Snet.Web.Client;
using LS.Snet.Web.Social.Vk;
using LS.Snet.Web.Social.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using LS.Snet.Web.Social.Ok;
using System.Security.Cryptography;
using LS.Snet.Web.Social.Facebook;

namespace LS.Snet.Web.Social
{
    public class AuthService
    {
        private readonly SocialConfig[] config;

        public AuthService(SocialConfig[] config)
        {
            this.config = config;
        }

        public AuthResult AuthVk(string access_token, Http.Configurator custom = null)
        {
            var social = config.GetConfig(SocialConfig.DefaultVk);
            var result = new AuthResult { Social = social };
            try
            {
                result.User = Http.Get($"{social.Api}/users.get", new { fields = "photo_100", access_token, v = social.Version })
                    .Configure(custom ?? new Http.Configurator())
                    .Run<VkResult<VkUser[]>>()
                    .Body.response[0];
            }
            catch (Exception ex)
            {
                result.Error = ex;
            }
            return result;
        }
        public AuthResult AuthGoogle(string code, Http.Configurator custom = null)
        {
            var social = config.GetConfig(SocialConfig.DefaultGoogle);
            var result = new AuthResult { Social = social };
            try
            {                
                var token = Http.Post($"{social.Api}/token", null, new
                {
                    code,
                    grant_type = "authorization_code",
                    client_id = social.Token["client_id"],
                    client_secret = social.Token["client_secret"],
                    redirect_uri = social.Url,
                    social.Version
                }).Configure(custom ?? new Http.Configurator()).Run<GoogleResult>().Body;
                if (token.error != null)
                    throw new Exception(token.error_description);
                var access_token = token.access_token;
                result.User = Http.Get($"{social.Api}/userinfo", new { social.Version, alt = "json", access_token })
                    .Configure(custom ?? new Http.Configurator()).Run<GoogleUser>().Body;
            }
            catch (Exception ex)
            {
                result.Error = ex;
            }
            return result;
        }
        public AuthResult AuthFacebook(string access_token, Http.Configurator custom = null)
        {
            var social = config.GetConfig(SocialConfig.DefaultFacebook);
            var result = new AuthResult { Social = social };
            try
            {                
                var response = Http.Get($"{social.Api}/me", new { access_token, fields = "first_name,last_name,picture" })
                    .Configure(custom ?? new Http.Configurator())
                    .Run<FacebookUser>();
                if (!response.Ok)
                    throw new Exception();
                result.User = response.Body;
            }
            catch (Exception ex)
            {
                result.Error = ex;
            }
            return result;
        }
        public AuthResult AuthOk(string code, Http.Configurator custom = null)
        {
            var social = config.GetConfig(SocialConfig.DefaultOk);
            var result = new AuthResult { Social = social };
            try
            {
                var application_secret_key = social.Token["application_secret_key"];
                var application_key = social.Token["application_key"];
                var application_id = social.Token["application_id"];
                var token = Http.Post($"{social.Api}/oauth/token.do", null, new { code, client_id = application_id, client_secret = application_secret_key, redirect_uri = social.Token["redirect_uri"], grant_type = "authorization_code" })
                    .Configure(custom ?? new Http.Configurator()).Run<OkToken>().Body;
                if (token.error != null)
                    throw new Exception(token.error_description);
                var access_token = token.access_token;
                var secret_key = SimpleHash<MD5>.Use(access_token + application_secret_key).Hex;
                var sig = SimpleHash<MD5>.Use($"application_key={application_key}format=jsonmethod=users.getCurrentUser{secret_key}").Hex;
                result.User = Http.Get($"{social.Api}/fb.do", new { application_key, format = "json", method = "users.getCurrentUser", sig, access_token })
                    .Configure(custom ?? new Http.Configurator())
                    .Run<OkUser>()
                    .Body;
            }
            catch (Exception ex)
            {
                result.Error = ex;
            }
            return result;
        }
    }
}
