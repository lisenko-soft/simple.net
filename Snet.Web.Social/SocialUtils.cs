﻿using System.Linq;

namespace LS.Snet.Web.Social
{
    public static class SocialUtils
    {
        public static SocialNet[] ViewModel(this SocialConfig[] configs) => configs.Select(x => x.ViewModel).ToArray();

        public static SocialConfig GetConfig(this SocialConfig[] config, SocialConfig @default)
        {
            if (!config.Any(x => x.Id == @default.Id))
                return @default;
            var social = config.Single(x => x.Id == @default.Id);
            if (social.Api == null)
                social.Api = @default.Api;
            if (social.Version == null)
                social.Version = @default.Version;
            return social;
        }
    }
}
