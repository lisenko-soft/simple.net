﻿using LS.Snet.Web.Client;
using System;
using System.Linq;

namespace LS.Snet.Web.Social
{
    public class NotifyService
    {
        private readonly SocialConfig[] config;

        public NotifyService(SocialConfig[] config)
        {
            this.config = config;
        }

        public Exception Send(string user_id, string message)
        {
            var parts = user_id.Split(':');
            if (parts.Length != 2)
                throw new ArgumentException(nameof(user_id));
            switch (parts[0])
            {
                case "vk":
                    return VkSend(parts[1], message);
                default:
                    throw new InvalidOperationException();
            }
        }

        public Exception VkSend(string user_id, string message)
        {
            var service = config.GetConfig(SocialConfig.DefaultVk);
            try
            {
                if (service.Token.ContainsKey("group"))
                    Http.Get($"{service.Api}/messages.send", new { access_token = service.Token["group"], v = service.Version, user_id, message }).Run();
                else
                    return new InvalidOperationException();
            }
            catch (Exception ex)
            {
                return ex;
            }
            return null;
        }
    }
}
